#!/bin/bash
apt-get update
apt-get upgrade -y
apt-get autoremove
curl -O http://vestacp.com/pub/vst-install.sh
bash vst-install.sh --nginx yes --apache yes --phpfpm no --named yes --remi yes --vsftpd yes \
--proftpd no --iptables yes --fail2ban yes --quota yes --exim yes --dovecot yes --spamassassin yes \
--clamav yes --softaculous yes --mysql yes --postgresql yes --hostname vesta.mt --email mtomekt@gmail.com \
--password 1qaz2wsx --interactive no --force
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install mc memcached php5.6 php7.0 php7.1 php7.2 php7.3 php*-cgi php*-common -y
apt-get install -y php*-json php*-redis php*-imagick php*-apcu php*-fpm php*-curl php*-mysql \
php*-gd php*-mcrypt php*-pgsql php*-sqlite3 php*-mbstring php*-dom php*-zip php*-intl \
php*-memcached php*-dev php*-uploadprogress php*-bz2 php*-imap php*-ldap php*-mongodb php*-solr php*-xdebug
apt-get install drush
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/bin --filename=composer
rm -f composer-setup.php
composer self-update
a2enmod cgi
cp -u ./vesta/usr/local/vesta/data/templates/web/apache2/* /usr/local/vesta/data/templates/web/apache2/
mkdir /usr/local/vesta/data/templates/web/skel/public_html/tools
cp -u ./vesta/usr/local/vesta/data/templates/web/skel/public_html/tools/info.php /usr/local/vesta/data/templates/web/skel/public_html/tools/info.php
apt-get install libapache2-mod-wsgi
a2enmod wsgi
cd /usr/local/vesta/data/templates/web
wget --no-check-certificate -O /usr/local/vesta/data/templates/web/apache2.tar.gz http://c.vestacp.com/0.9.8/ubuntu/wsgi/apache2.tar.gz
tar -xzvf /usr/local/vesta/data/templates/web/apache2.tar.gz -C /usr/local/vesta/data/templates/web/
rm -f /usr/local/vesta/data/templates/web/apache2.tar.gz
sed -e 's|-u memcache|-u www-data|g' -i /etc/memcached.conf
sed -e 's|-m 64|-m 128|g' -i /etc/memcached.conf
/etc/init.d/memcached restart
service apache2 restart
service vesta restart
mkdir /home/c
umount /home/c
mount -t cifs -o username=admin,password=1,rw,dir_mode=0777,file_mode=0666 //192.168.1.150/etc /home/c
IPI=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')
HOST_LINE="${IPI} vesta.mt"
echo "$HOST_LINE" >> /home/c/hosts
umount /home/c
cp -u vesta/usr/local/vesta/bin/v-add-web-domain /usr/local/vesta/bin/v-add-web-domain
service vesta restart
sed -e 's|PATH="\(.*\)"|PATH="/usr/local/vesta/bin:\1"|g' -i /etc/environment
/etc/profile.d/vesta.sh
/usr/local/vesta/bin/v-add-domain admin vredmine.mt
