#!/usr/bin/env bash

drush dl drupal
mv ./drupal*/* ./
mv ./drupal*/.* ./
rm -rf ./drupal*/
echo -e "Please type db name:";
read DBN
echo -e "Please type site name:";
read SITE

drush site-install standard --site-name="${SITE}" \
--account-name=admin \
--account-pass=admin \
--db-url=mysql://${DBN}:${DBN}@localhost/${DBN} -y

mkdir sites/all/modules/contrib
mkdir sites/all/modules/custom

drush dl devel
drush dl coder